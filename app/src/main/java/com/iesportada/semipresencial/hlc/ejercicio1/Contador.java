package com.iesportada.semipresencial.hlc.ejercicio1;

public class Contador {
    private int cafes;
    private int tiempo;
    private  static final int DESCANSO = 5;

    public Contador() {
        this.setCafes(0);
        this.tiempo = DESCANSO;
    }

    public Contador(int c, int t) {
        this.setCafes(c);
        this.tiempo = t;
    }

    public String aumentarTiempo(){
        this.tiempo += 1;
        return String.valueOf(this.tiempo) + ":00";
    }

    public String disminuirTiempo(){
        this.tiempo -= 1;
        if (this.tiempo < 1)
            tiempo = 1;
        return String.valueOf(this.tiempo) + ":00";
    }

    public String aumentarCafes(){
        this.setCafes(this.getCafes() + 1);
        return String.valueOf(this.getCafes());
    }

    public int getTiempo(){
        return  this.tiempo;
    }

    public int getCafes() {
        return cafes;
    }

    public void setCafes(int cafes) {
        this.cafes = cafes;
    }
}
