package com.iesportada.semipresencial.hlc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.iesportada.semipresencial.hlc.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        Button buttonEJ1 = findViewById(R.id.buttonEjercicio1);
        Button buttonEJ2 = findViewById(R.id.buttonEjercicio2);
        Button buttonEJ3 = findViewById(R.id.buttonEjercicio3);

        buttonEJ1.setOnClickListener(v -> {
            Intent intent = new Intent(this, com.iesportada.semipresencial.hlc.ejercicio1.MainActivity.class);
            startActivity(intent);
        });

        buttonEJ2.setOnClickListener(v-> {
            Intent intent = new Intent(this, com.iesportada.semipresencial.hlc.ejercicio2.MainActivity.class);
            startActivity(intent);
        });

        buttonEJ3.setOnClickListener(v-> {
            Intent intent = new Intent(this, com.iesportada.semipresencial.hlc.ejercicio3.MainActivity.class);
            startActivity(intent);
        });
    }
}