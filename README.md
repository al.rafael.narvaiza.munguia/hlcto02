# HLC TO 02

### Ejercicio 1

Partimos del ejemplo de DAM.ORG.ES para cumplir con los requisitos del ejercicio, realizamos:
· Añadir un clip de sonido utilizando la clase MediaPlayer
· Controlar el flujo de información para no poder reiniciar el contador más veces de los cafés estipulados.
· Lanzar el seteo del conteo de cafés en el onClick, en vez de cuando termine la cuenta atrás.
· Lanzar un popUp cuando lleguemos al límite de cafés y a su vez habilitar el botón de Reset.


### Ejercicio 2

Creamos una actividad con un campo de texto y dos botones. A través del bindeo, seteamos la lógica de los botones al Layout.
El primer botón se encarga de levantar un intent a través de *ACTION_VIEW* y pasar una URL para llamar al navegador predeterminado del SO.
El segundo botón se encarga de levantar un intent a través de la clase browser, que utiliza **WebView** para llamar a esta nueva vista y de forma embebida, browsear la url que se le pasa como un string.

### Ejercicio 3

Creamos dos campos de texto que permiten introducir números. Controlamos el flujo de información para evitar *crashes* a través de excepciones de formato numérico. También realizamos los siguientes controles:
· Descartamos *inputs* en los textfield si son vacios o nulos.
· Descartamos los valores de números no naturales.
· Descartamos realizar la acción de coleccionar números primos si la colección no está vacía. De esta forma mantenemos la integridad de la colección.

