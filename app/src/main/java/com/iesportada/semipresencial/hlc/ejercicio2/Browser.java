package com.iesportada.semipresencial.hlc.ejercicio2;

/**
 * @author Rafael Narvaiza
 */

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.iesportada.semipresencial.hlc.R;

public class Browser extends AppCompatActivity {


    private static String url ="";
    WebView myWB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_view);

        myWB = findViewById(R.id.webView);


        myWB.loadUrl(getUrl());

        WebSettings ws = myWB.getSettings();
        ws.setJavaScriptEnabled(true);
        Toast.makeText(Browser.this, getUrl(), Toast.LENGTH_SHORT).show();

    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String direccion) {
        url = direccion;
    }
}