package com.iesportada.semipresencial.hlc.ejercicio3

/**
 * @author Rafael Narvaiza
 */

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.iesportada.semipresencial.hlc.R
import java.lang.NumberFormatException
import java.util.*

class MainActivity : AppCompatActivity() {

    val primosColeccion = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main4)

        val firstNumber = findViewById<EditText>(R.id.editTextFirstNumber)
        val secondNumber = findViewById<EditText>(R.id.editTextSecondNumber)
        val calculateButton = findViewById<Button>(R.id.buttonComprobar)

        calculateButton.setOnClickListener(View.OnClickListener {

            /**
             * Here we use a NumberFormatException to avoid crashing due to chars input
             */
            try {
                val fromNumber: Int = firstNumber.text.toString().toInt()
                val toNumber: Int = secondNumber.text.toString().toInt()
                /**
                 * Here we try to discard all null or empty fields to avoid compare zero or null values.
                 */
                if(firstNumber.text.toString().isNullOrBlank() || secondNumber.text.toString().isNullOrBlank()){
                    Toast.makeText(this, "Por favor, introduzca números en los apartados.", Toast.LENGTH_SHORT).show()
                }
                else{
                    /**
                     * Here we finish the data manging to avoid errors owing to the nature of the input.
                     * Prime numbers are a group of numbers inside natural numbers. So we need to avoid numbers below zero and zero.
                     */
                    if(fromNumber<1 || toNumber<1){
                        Toast.makeText(this, "Los números primos solo pueden ser naturales. Haz el favor de poner números mayores que cero", Toast.LENGTH_SHORT).show()
                    }
                    else{

                        /**
                         * We check if first number is bigger than second. In this case we show a warning toast.
                         */
                        if(toNumber>fromNumber){
                            /**
                             * We finally start to check the numbers, only if the collection has been cleaned before
                             */
                            while (primosColeccion.isEmpty()){
                                    var x = fromNumber
                                    while (x <= toNumber) {
                                        if (esPrimo(x)) {
                                            primosColeccion.add(x)
                                        }
                                        x++
                                    }
                            }
                            Toast.makeText(
                                    this,
                                    primosColeccion.toString(),
                                    Toast.LENGTH_LONG
                            ).show()
                        }
                        else{
                            Toast.makeText(this, "El primer número ha de ser menor que el segundo", Toast.LENGTH_SHORT).show()
                        }

                    }
                }
            } catch (e: NumberFormatException) {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
            primosColeccion.clear()
        })
        }

    private fun esPrimo(numero: Int): Boolean {
        var esPrimo : Boolean
        esPrimo = true

        var i = 2
        while (i <= Math.sqrt(numero.toDouble()) && esPrimo) {
            if (numero % i == 0) {
                esPrimo = false
            }
            i++
        }
        return esPrimo
    }
}









