package com.iesportada.semipresencial.hlc.ejercicio2;

/**
 * @author Rafael Narvaiza
 */

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.material.textfield.TextInputEditText;
import com.iesportada.semipresencial.hlc.R;
import com.iesportada.semipresencial.hlc.databinding.ActivityMain3Binding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMain3Binding binding;
    private String url;

    TextInputEditText tiet;
    Button buttonNavegador;
    Button buttonWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        binding = ActivityMain3Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        tiet = findViewById(R.id.textViewInputUrl);
        buttonWebView = findViewById(R.id.buttonWebView);
        buttonNavegador = findViewById(R.id.buttonNavegador);

        buttonNavegador.setOnClickListener(view1 -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tiet.getText().toString()));
            startActivity(intent);
        });

        buttonWebView.setOnClickListener(view1 -> {

            Intent intent = new Intent(MainActivity.this, Browser.class);
            Browser.setUrl(tiet.getText().toString());
            startActivity(intent);
        });

    }

    @Override
    public void onClick(View view) {
        url = tiet.getText().toString();
        openWebPage(url);

    }

    public void openWebPage(String url){
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null ){
            startActivity(intent);
        }
    }




}